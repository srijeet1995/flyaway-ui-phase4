/**
*	Used to load services as a package
*
*	
*/
define([
	'./common',
	'./interface',
	'./airports',
	'./bookings',
	'./search',
	'./flights'
], function(){});
