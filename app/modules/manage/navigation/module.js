/**
*	Defines booking module
*
*	
*/
define([
	'angular',
	'core/main'
], function( angular ){
	var module = angular.module('BookFlight.manage.navigation', [
		'BookFlight.core'
	]);

	return module;
});