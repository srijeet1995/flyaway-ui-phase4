/**
*	Entry point for the manage.airports module
*
*	
*/
define([
	'manage/airports/module',
	'manage/airports/add/addCtrl',
	'manage/airports/remove/removeCtrl'
], function(){});