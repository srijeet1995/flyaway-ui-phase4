/**
*	Defines booking module
*
*	
*/
define([
	'angular',
	'core/main'
], function( angular ){
	var module = angular.module('BookFlight.manage.airports', [
		'BookFlight.core'
	]);

	return module;
});