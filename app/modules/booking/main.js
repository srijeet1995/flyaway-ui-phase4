/**
*	Entry point for the booking module
*
*	
*/
define([
	'booking/module',
	'booking/cancel/cancelCtrl',
	'booking/new/newCtrl',
	'booking/search/searchCtrl',
	'booking/flight_status/flightStatusCtrl'
], function(){});
