/**
*	Defines header module
*
*	@author Lakha Singh
*/
define([
	'angular',
	'core/module'
], function( angular ){
	var module = angular.module('FlyAway.header', ['FlyAway.core']);

	return module;
});